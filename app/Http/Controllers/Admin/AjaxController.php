<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AjaxController extends Controller
{
    public function doneesCity(Request $request) {
        $term = $request->input('term');
        $options = \App\Models\Donee::where('city', 'like', '%'.$term.'%')->distinct('city')->get();
        return $options->pluck('city', 'id');
      }
}
