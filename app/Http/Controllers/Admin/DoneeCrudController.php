<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\DoneeRequest as StoreRequest;
use App\Http\Requests\DoneeRequest as UpdateRequest;

class DoneeCrudController extends CrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Donee');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/donees');
        $this->crud->setEntityNameStrings('donee', 'donees');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        $this->crud->setFromDb();

        // ------ CRUD FIELDS
        // $this->crud->addField($options, 'update/create/both');
        // $this->crud->addFields($array_of_arrays, 'update/create/both');
        // $this->crud->removeField('name', 'update/create/both');
        // $this->crud->removeFields($array_of_names, 'update/create/both');

        // ------ CRUD COLUMNS
        // $this->crud->addColumn(); // add a single column, at the end of the stack
        // $this->crud->addColumns(); // add multiple columns, at the end of the stack
        // $this->crud->removeColumn('kidney'); // remove a column from the stack
        $this->crud->removeColumns(['kidney', 'disabled', 'User_id', 'action']); // remove an array of columns from the stack
        $this->crud->removeButton( 'preview' );
$this->crud->removeButton( 'revisions' );
$this->crud->removeAllButtons();
        // $this->crud->setColumnDetails('created', ['name' => 'created', 'label' => 'Created At', 'type' => "datetime"]); // adjusts the properties of the passed in column (by name)
        $this->crud->setColumnDetails('kidneys', ['name' => 'kidneys', 'label' => 'Kidneys', 'type' => "check"]); // adjusts the properties of the passed in column (by name)
        $this->crud->setColumnDetails('pancreas', ['name' => 'pancreas', 'label' => 'Pancreas', 'type' => "check"]); // adjusts the properties of the passed in column (by name)
        $this->crud->setColumnDetails('lungs', ['name' => 'lungs', 'label' => 'Lungs', 'type' => "check"]); // adjusts the properties of the passed in column (by name)
        $this->crud->setColumnDetails('corneas', ['name' => 'corneas', 'label' => 'Corneas', 'type' => "check"]); // adjusts the properties of the passed in column (by name)
        $this->crud->setColumnDetails('liver', ['name' => 'liver', 'label' => 'Liver', 'type' => "check"]); // adjusts the properties of the passed in column (by name)
        $this->crud->setColumnDetails('heart', ['name' => 'heart', 'label' => 'Heart', 'type' => "check"]); // adjusts the properties of the passed in column (by name)
        // $this->crud->setColumnDetails('dob', ['name' => 'dob', 'label' => 'Date of Birth', 'type' => "date"]); // adjusts the properties of the passed in column (by name)
        
        
        $this->crud->addFilter([ // add a "simple" filter called Draft 
            'type' => 'simple',
            'name' => 'kidneys',
            'label'=> 'Kidneys'
        ],
        false, // the simple filter has no values, just the "Draft" label specified above
        function() { // if the filter is active (the GET parameter "draft" exits)
            $this->crud->addClause('where', 'kidneys', 'true'); 
        });
        
        $this->crud->addFilter([ // add a "simple" filter called Draft 
            'type' => 'simple',
            'name' => 'pancreas',
            'label'=> 'Pancreas'
        ],
        false, // the simple filter has no values, just the "Draft" label specified above
        function() { // if the filter is active (the GET parameter "draft" exits)
            $this->crud->addClause('where', 'pancreas', 'true'); 
        });
        
        $this->crud->addFilter([ // add a "simple" filter called Draft 
            'type' => 'simple',
            'name' => 'lungs',
            'label'=> 'lungs'
        ],
        false, // the simple filter has no values, just the "Draft" label specified above
        function() { // if the filter is active (the GET parameter "draft" exits)
            $this->crud->addClause('where', 'lungs', 'true'); 
        });
        
        $this->crud->addFilter([ // add a "simple" filter called Draft 
            'type' => 'simple',
            'name' => 'corneas',
            'label'=> 'Corneas'
        ],
        false, // the simple filter has no values, just the "Draft" label specified above
        function() { // if the filter is active (the GET parameter "draft" exits)
            $this->crud->addClause('where', 'corneas', 'true'); 
        });
        
        $this->crud->addFilter([ // add a "simple" filter called Draft 
            'type' => 'simple',
            'name' => 'heart',
            'label'=> 'Heart'
        ],
        false, // the simple filter has no values, just the "Draft" label specified above
        function() { // if the filter is active (the GET parameter "draft" exits)
            $this->crud->addClause('where', 'heart', '1'); 
        });
        
        $this->crud->addFilter([ // add a "simple" filter called Draft 
            'type' => 'simple',
            'name' => 'liver',
            'label'=> 'Liver'
        ],
        false, // the simple filter has no values, just the "Draft" label specified above
        function() { // if the filter is active (the GET parameter "draft" exits)
            $this->crud->addClause('where', 'liver', 'true'); 
        });
        
        $this->crud->addFilter([ // dropdown filter
            'name' => 'city',
            'type' => 'dropdown',
            'label'=> 'City'
          ], [
            'New Delhi' => 'New Delhi',
            'KOLKATA' => 'KOLKATA',
            'Indore' => 'Indore',
            'Mumbai' => 'Mumbai',
            'Naihatii' => 'Naihatii',
            'uttarkashi' => 'uttarkashi',
            'Nashik' => 'Nashik',
            'Delhi' => 'Delhi',
            'pune' => 'pune',
            'RANCHI' => 'RANCHI',
            'Faridabad' => 'Faridabad',
            'LUCKNOW' => 'LUCKNOW',
            'Chennai' => 'Chennai',
            'Hyderabad' => 'Hyderabad',
            'DINDIGUL' => 'DINDIGUL',
            'noida' => 'noida',
            'Dharwad' => 'Dharwad',
            'Bangalore' => 'Bangalore',
            'Navi Mumbai' => 'Navi Mumbai',
            'Deoghar' => 'Deoghar',
            'belgaum' => 'belgaum',
            'patna' => 'patna',
            'Rewa' => 'Rewa',
            'Panchkula' => 'Panchkula',
            'ghaziabad' => 'ghaziabad',
            'Ahmedabad' => 'Ahmedabad',
            'silvassa' => 'silvassa',
            'gorakhpur' => 'gorakhpur',
            'Mangalore' => 'Mangalore',
            'LUDHIANA' => 'LUDHIANA',
            'OTTAPALAM' => 'OTTAPALAM',
            'kanchipuram' => 'kanchipuram',
            'Contai' => 'Contai',
            'TIRUCHIRAPALLI' => 'TIRUCHIRAPALLI',
            'Jalna' => 'Jalna',
            'Nuzvid' => 'Nuzvid',
            'gaziabad' => 'gaziabad',
            'qadian' => 'qadian',
            'muzaffarnager' => 'muzaffarnager',
            'Kovilpatti' => 'Kovilpatti',
            'Agra' => 'Agra',
            'Nagpur' => 'Nagpur',
            'Bengaluru' => 'Bengaluru',
            'Haveri' => 'Haveri',
            'RAEBARELI' => 'RAEBARELI',
            'Surat' => 'Surat',
            'balasore' => 'balasore',
            'Kakinada' => 'Kakinada',
            'Darjeeling' => 'Darjeeling',
            'Aurangabad' => 'Aurangabad',
            'Jammu' => 'Jammu',
            'Raigad' => 'Raigad',
            'AHEMDABAD' => 'AHEMDABAD',
            'Chatra' => 'Chatra',
            'Sikar' => 'Sikar',
            'ahmednagar' => 'ahmednagar',
            'CHAKRADHARPUR' => 'CHAKRADHARPUR',
            'thrissur' => 'thrissur',
            'tohana' => 'tohana',
            'Gurgaon' => 'Gurgaon',
            'krishnagiri' => 'krishnagiri',
            'vishakapatnam' => 'vishakapatnam',
            'Rajahmundry' => 'Rajahmundry',
            'Cuttack' => 'Cuttack',
            'karachi' => 'karachi',
            'Ambikapur' => 'Ambikapur',
             ''=>' ',
            'Sitapur' => 'Sitapur',
            'asldkj' => 'asldkj',
            'Philadelphia' => 'Philadelphia',
            'Ambernath' => 'Ambernath',
            'DEHRADUN' => 'DEHRADUN',
            'Bhubaneswar' => 'Bhubaneswar',
            'Vadodara' => 'Vadodara',
            'VARANASI' => 'VARANASI',
            'Bikaner' => 'Bikaner',
            'Villupuram' => 'Villupuram',
            'Paradeep' => 'Paradeep',
            'bhopal' => 'bhopal',
            'Bhadohi' => 'Bhadohi',
            'Punr' => 'Punr',
            'Karimnagar' => 'Karimnagar',
            'Ferozepur' => 'Ferozepur',
            'Raibreli' => 'Raibreli',
            'Kapurthla' => 'Kapurthla',
            'Mira Road' => 'Mira Road',
            'Papanasam' => 'Papanasam',
            'Akola' => 'Akola',
            'pathankot' => 'pathankot',
            'Jaipur' => 'Jaipur',
            'Trivandrum' => 'Trivandrum',
            'Raibareli' => 'Raibareli',
            'Pratapgarh' => 'Pratapgarh',
            'CHANDIGARH' => 'CHANDIGARH',
            'Guwahati' => 'Guwahati',
            'Rajkot' => 'Rajkot',
            'Alwal' => 'Alwal',
            'kalyani' => 'kalyani',
            'Eluru' => 'Eluru',
            'Cooch Behar' => 'Cooch Behar',
            'BHOPAL- 462001' => 'BHOPAL- 462001',
            'Ballabgarh Faridabad' => 'Ballabgarh Faridabad',
            'Nasik' => 'Nasik',
            'Fatehabad' => 'Fatehabad',
            'Itanagar' => 'Itanagar',
            'NAGAON' => 'NAGAON',
            'Kolhapur' => 'Kolhapur',
            'Zirakpur' => 'Zirakpur',
            'Kanpur' => 'Kanpur',
            'Muzaffarnagar' => 'Muzaffarnagar',
            'Jalandhar' => 'Jalandhar',
            'Ajmer' => 'Ajmer',
            'Parwanoo' => 'Parwanoo',
            'Hisar' => 'Hisar',
            'NALGONDA' => 'NALGONDA',
            'thane' => 'thane',
            'Ulhasnagar' => 'Ulhasnagar',
            'Moradabad' => 'Moradabad',
            'Amravati' => 'Amravati',
            'Tharad' => 'Tharad',
            'chennnai' => 'chennnai',
            'BATHINDA' => 'BATHINDA',
            'Kapurthala' => 'Kapurthala',
            'Burdwan' => 'Burdwan',
            'bolpur' => 'bolpur',
            'MANKAPUR' => 'MANKAPUR',
            ], function($value) { // if the filter is active
              $this->crud->addClause('where', 'city', $value);
          });

          $this->crud->addFilter([ // dropdown filter
            'name' => 'gender',
            'type' => 'dropdown',
            'label'=> 'Gender'
          ], [
            'male' => 'Male',
            'female' => 'Female',
          ], function($value) { // if the filter is active
              $this->crud->addClause('where', 'gender', $value);
          });
          
          $this->crud->addFilter([ // dropdown filter
            'name' => 'bgroup',
            'type' => 'dropdown',
            'label'=> 'Blood Group'
          ], [
            'abminus' => 'AB -ve',
            'amplus' => 'AB +ve',
            'aplus' => 'A +ve',
            'aminus' => 'A -ve',
            'bplus' => 'B +ve',
            'bminus' => 'B -ve',
            'oplus' => 'O +ve',
            'ominus' => 'O -ve',

          ], function($value) { // if the filter is active
              $this->crud->addClause('where', 'bgroup', $value);
          });
          
        
        // $this->crud->setColumnsDetails(['column_1', 'column_2'], ['attribute' => 'value']);

        // ------ CRUD BUTTONS
        // possible positions: 'beginning' and 'end'; defaults to 'beginning' for the 'line' stack, 'end' for the others;
        // $this->crud->addButton($stack, $name, $type, $content, $position); // add a button; possible types are: view, model_function
        // $this->crud->addButtonFromModelFunction($stack, $name, $model_function_name, $position); // add a button whose HTML is returned by a method in the CRUD model
        // $this->crud->addButtonFromView($stack, $name, $view, $position); // add a button whose HTML is in a view placed at resources\views\vendor\backpack\crud\buttons
        // $this->crud->removeButton($name);
        // $this->crud->removeButtonFromStack($name, $stack);
        // $this->crud->removeAllButtons();
        // $this->crud->removeAllButtonsFromStack('line');

        // ------ CRUD ACCESS
        // $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete']);
        // $this->crud->denyAccess(['list', 'create', 'update', 'reorder', 'delete']);

        // ------ CRUD REORDER
        // $this->crud->enableReorder('label_name', MAX_TREE_LEVEL);
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('reorder');

        // ------ CRUD DETAILS ROW
        // $this->crud->enableDetailsRow();
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('details_row');
        // NOTE: you also need to do overwrite the showDetailsRow($id) method in your EntityCrudController to show whatever you'd like in the details row OR overwrite the views/backpack/crud/details_row.blade.php

        // ------ REVISIONS
        // You also need to use \Venturecraft\Revisionable\RevisionableTrait;
        // Please check out: https://laravel-backpack.readme.io/docs/crud#revisions
        // $this->crud->allowAccess('revisions');

        // ------ AJAX TABLE VIEW
        // Please note the drawbacks of this though:
        // - 1-n and n-n columns are not searchable
        // - date and datetime columns won't be sortable anymore
        // $this->crud->enableAjaxTable();

        // ------ DATATABLE EXPORT BUTTONS
        // Show export to PDF, CSV, XLS and Print buttons on the table view.
        // Does not work well with AJAX datatables.
        // $this->crud->enableExportButtons();

        // ------ ADVANCED QUERIES
        // $this->crud->addClause('active');
        // $this->crud->addClause('type', 'car');
        // $this->crud->addClause('where', 'name', '==', 'car');
        // $this->crud->addClause('whereName', 'car');
        // $this->crud->addClause('whereHas', 'posts', function($query) {
        //     $query->activePosts();
        // });
        // $this->crud->addClause('withoutGlobalScopes');
        // $this->crud->addClause('withoutGlobalScope', VisibleScope::class);
        // $this->crud->with(); // eager load relationships
        // $this->crud->orderBy();
        // $this->crud->groupBy();
        // $this->crud->limit();
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
