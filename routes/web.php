<?php

use Illuminate\Support\Carbon;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('admin');
});

Route::group(['prefix' => 'admin', 'middleware' => ['admin'], 'namespace' => 'Admin'], function() {
    CRUD::resource('donees', 'DoneeCrudController');
    CRUD::resource('donors', 'DonorCrudController');

    Route::get('ajax/donees-city', 'AjaxController@doneesCity');
    // CRUD::resource('test', 'TestCrudController');

});


Route::get('update', function() {
    \App\Models\Donee::chunk(100, function($donees) {
        foreach($donees as $donee) {
            // Carbon::$donee->created
        }
    });
});